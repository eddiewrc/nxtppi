# WHAT IS NXTppi #

NXTPPI is a Neural Network based data fusion method for the prediction of Protein-Protein Interactions (PPI) at the scale of the human interactome. This software is the stand-alone version of the tool we built with the NXTfusion library, which will be soon available as beta version.

### What is this repository for? ###

The code here contains a standalone version of NXTppi (`NXTppi.predictor.py`), which takes as input a file containing pairs of proteins, identified by Uniprot accession numbers, and outputs the likelihood of each pair to interact.
An example of a properly formatted file is the `braunInput.txt`.

### How do I set it up? ###

Droppler has some dependencies, which are popular pytohn libraries (such as pytorch, numpy and scipy). 
Here we show how to create a miniconda environment containig all those libraries. Similar instructions can be used to install the dependencies with `pip`. 
Droppler runs on python 3 .

* Download and install miniconda from `https://docs.conda.io/en/latest/miniconda.html`
* Create a new conda environment by typing: `conda create -n nxtfusion -python=2.7`
* Enter the environment by typing: `conda activate nxtfusion`
* Install pytorch >= 1.0 with the command: `conda install pytorch -c pytorch` or refer to pytorch website https://pytorch.org
* Install scipy with the command: `conda install scipy`
	
You can remove this environment at any time by typing: conda remove -n nxtfusion --all
 

### What is this repository contains? ###

* `NXTppi.predictor.py` -> is the standalone predictor.
* `PPImodels.py` -> Contains the model used by NXTppi.
* `NXTfusion/` -> folder containing core source code necessary to run NXTppi
* `marshalled/ , models/` -> folders containing some serialized data and the trained models
* `braunInput.txt.md` -> example of input file
* `README.md` -> this readme

### What is the input file format? ###

The input file format is the following. The first two columns are Uniprot accession number of proteins and the third is the label.
The program will *ignore* the label and provide its predictions.
```
P12004  P39748  1
O15195  P40425  0
P21860  Q02297  1
P52298  Q09161  1
P42575  P78560  1
P34897  Q96MF2  0
P02545  P06400  1
P60033  P61916  0
O15530  P31749  1
P98179  Q15459  0
P18754  Q8WZ60  0
P63244  Q08499  1
P29590  Q96BR1  0
```

### How do I predict proteins with NXTppi? ###
The input command line is the following
```
python NXTppi.predictor.py INPUT_FILE models/TRAINED_MODEL DEVICE
```
where device can be for example cpu:0 or cuda:0.

You can predict the example input file by typing:

```
python NXTppi.predictor.py braunInput.txt models/G2.model50iter.t cpu:0
```

The predictions will be available in `braunInput.txt.nxPredictions`.

### Who do I talk to? ###

Please report bugs at the following mail address:
daniele DoT raimondi aT kuleuven DoT be

