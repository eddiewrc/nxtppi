import cPickle, os, sys, gc, marshal, copy, random, time, socket, cPickle, math
import NXTfusion.NXFeaturesConstruction as FC
from sys import stdout
import numpy as np
import torch as t
from NXTfusion import NXTfusion as NX
from NXTfusion.NXmultiRelSide import NNwrapper
from PPImodels import MultiRelLayerNormModel
IGNORE_INDEX = -99
NUM_WORKERS = 0 
cacheDir = "./marshalled/"


def parseArgs(args):
	print "Arguments: " , args
	if len(args) != 4 or "-h" in args[1]:
		print ""
		print "USAGE: python %s INPUT_FILE, TRAINED_MODEL, DEVICE" % (args[0])
		print "\n\tDEVICE can be cuda:X or cpu:X (e.g. cuda:0, cpu:0)\n"
		sys.exit(1)
	FILENAME = args[1]
	MODEL = args[2]
	DEVICE = args[3]
	return FILENAME, MODEL, DEVICE

def main(args):
	print "NXTppi predictor version 1\n"
	FILENAME, MODEL, DEVICE = parseArgs(args)
	print "NAME: ",  FILENAME
	NAME = MODEL
	print "Model: %s, DEVICE: %s" % (str(MODEL), DEVICE)
	print "Loading data..."
	protList = list(cPickle.load(open(cacheDir+"protList.cPickle")))
	print "Creating entities..."
	protEnt = NX.Entity("proteins", protList, dtype=np.int16)
	print "Loading completed."
	testdata = readInputFile(FILENAME, protEnt)
	X, Y, corresp = FC.buildPytorchFeats(testdata, protEnt, protEnt)
	model = t.load(NAME)
	wrapper = NNwrapper(model, dev=DEVICE, ignore_index = IGNORE_INDEX, nworkers = NUM_WORKERS)
	
	Yp = wrapper.predict(X, None, None)
	#U.getScoresSVR(Yp, Y, threshold=None, PRINT = True, CURVES = False, SAVEFIG=None)
	writePreds(FILENAME+".nxPredictions", corresp, Y, Yp)
	print "Predictions have been stored in "+FILENAME+".nxPredictions .\nDone."
	return 0
	
def writePreds(f, x, y, yp):
	ofp = open(f, "w")
	i = 0
	assert len(x) == len(y) == len(yp)
	while i < len(x):
		ofp.write("%s\t%s\t%d\t%2.3f\n" % (x[i][0], x[i][1], y[i], yp[i]))
		i+=1
	ofp.close()
	
def readInputFile(F, protEnt):
	ifp = open(F)
	lines = ifp.readlines()
	ifp.close()
	db = {}
	idlist = set()
	for l in lines:
		tmp = l.strip().split()
		db[tuple(sorted([tmp[0], tmp[1]]))] = 1
		idlist.add(tmp[0])
		idlist.add(tmp[1])
		assert protEnt.has_key(tmp[0])
		assert protEnt.has_key(tmp[1])
	return db

if __name__ == '__main__':
	import sys
	sys.exit(main(sys.argv))

