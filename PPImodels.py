	
import torch as t
from torch.autograd import Variable
import os
from NXTfusion.NXmodels import NXmodelProto

class MultiRelLayerNormModel(NXmodelProto):
	def __init__(self, ERG, name = "MultiRelLayerNorm"):
		super(MultiRelLayerNormModel, self).__init__()
		os.system("mkdir -p models")
		self.name = name
		##########DEFINE NN HERE##############
		TISS_LATENT_SIZE = 10
		SIDE_LEN_PROT = 20
		PROT_LATENT_SIZE = 70
		PF_LATENT_SIZE = 50
		DIS_LATENT_SIZE = 50
		ACTIVATION = t.nn.Tanh()
		ACTIV_REG = t.nn.Tanh()
		if "prot-prot" in ERG:
			protEmbLen = ERG["prot-prot"]["lenDomain1"]
			self.protEmb = t.nn.Embedding(protEmbLen, PROT_LATENT_SIZE)
			self.protHid = t.nn.Sequential(t.nn.Linear(PROT_LATENT_SIZE, 50), t.nn.LayerNorm(50), ACTIVATION)
			self.sideHid = t.nn.Sequential(t.nn.Linear(SIDE_LEN_PROT, 20), t.nn.LayerNorm(20), ACTIVATION, t.nn.Linear(20,20), t.nn.LayerNorm(20), ACTIVATION)
			self.sideMix1 = t.nn.Bilinear(PROT_LATENT_SIZE, 20, PROT_LATENT_SIZE)
			self.biProtProt = t.nn.Bilinear(50, 50, 10)
			self.outProtProt = t.nn.Sequential( t.nn.LayerNorm(10), ACTIVATION, t.nn.Dropout(0.1), t.nn.Linear(10,7))
		
		if "prot-expr" in ERG:
			tissEmbLen = ERG["prot-expr"]["lenDomain2"]
			self.tissEmb = t.nn.Embedding(tissEmbLen, TISS_LATENT_SIZE)
			self.tissHid = t.nn.Sequential(t.nn.Linear(TISS_LATENT_SIZE, 50), t.nn.LayerNorm(50), ACTIVATION)
			self.biProtTiss = t.nn.Bilinear(50, 50, 10)
			self.outProtTiss = t.nn.Sequential(LayerNorm(10), ACTIV_REG, t.nn.Dropout(0.1), t.nn.Linear(10,1)	)
		
		if "prot-dom" in ERG:
			domEmbLen = ERG["prot-dom"]["lenDomain2"]
			self.domEmb = t.nn.Embedding(domEmbLen, PF_LATENT_SIZE)
			self.domHid = t.nn.Sequential(t.nn.Linear(PF_LATENT_SIZE, 50), t.nn.LayerNorm(50), t.nn.Tanh())
			self.biProtDom = t.nn.Bilinear(50, 50, 10)
			self.outProtDom = t.nn.Sequential(t.nn.LayerNorm(10), ACTIVATION, t.nn.Dropout(0.1), t.nn.Linear(10,1))
		
		if "prot-dis" in ERG:
			disEmbLen = ERG["prot-dis"]["lenDomain2"]
			self.disEmb = t.nn.Embedding(disEmbLen, DIS_LATENT_SIZE)
			self.disHid = t.nn.Sequential(t.nn.Linear(DIS_LATENT_SIZE, 50), t.nn.LayerNorm(50), ACTIVATION)
			self.biProtDis = t.nn.Bilinear(50, 50, 10)
			self.outProtDis = t.nn.Sequential(t.nn.LayerNorm(10), ACTIVATION, t.nn.Dropout(0.1), t.nn.Linear(10,1))
		self.apply(self.init_weights)

	def forward(self, relName, i1, i2, s1=None, s2=None):
		if relName == "prot-prot":
			if type(s1) != type(None):
				u = self.sideMix1(self.protEmb(i1), self.sideHid(s1))
			else:
				u = self.protEmb(i1)
			if type(s2) != type(None):
				v = self.sideMix1(self.protEmb(i2), self.sideHid(s2))
			else:
				v = self.protEmb(i2)
			u = self.protHid(u).squeeze()
			v = self.protHid(v).squeeze()
			o = self.biProtProt(u, v)
			o = self.outProtProt(o)		
			return o
		if relName == "prot-expr":
			u = self.protEmb(i1)
			v = self.tissEmb(i2)
			u = self.protHid(u).squeeze()
			v = self.tissHid(v).squeeze()
			o = self.biProtTiss(u, v)
			o = self.outProtTiss(o)
			return o
		if relName == "prot-dis":
			u = self.protEmb(i1)
			v = self.disEmb(i2)
			u = self.protHid(u).squeeze()
			v = self.disHid(v).squeeze()
			o = self.biProtDis(u, v)
			o = self.outProtDis(o)
			return o
		if relName == "prot-dom":
			u = self.protEmb(i1)
			v = self.domEmb(i2)
			u = self.protHid(u).squeeze()
			v = self.domHid(v).squeeze()
			o = self.biProtDom(u, v)
			o = self.outProtDom(o)
			return o
