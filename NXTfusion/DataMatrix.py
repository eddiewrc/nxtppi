
import NXFeaturesConstruction as NFC
import cPickle
import numpy as np
from multipledispatch import dispatch
import time
from Nexis import Entity


class SideInfo(object):

	"""

	"""
	@dispatch(str, Entity, dict)
	def __init__(self, name, ent1, data):
		self.name = name
		self.ent1 = ent1
		self.dtype = type
		self.data = {} # data = {idx:[features]}
		missing = 0
		l = len(data.values()[0])
		for i in ent1:
			try:
				self.data[ent1[i]] = data[i]
			except:
				self.data[ent1[i]] = [0]*l
				missing += 1
		print "Missing: ", missing
		for i in self.data.items():
			assert len(i[1]) == len(self.data.values()[0])
	
	@dispatch( str)
	def __init__(self, path):
		print "Loading %s..." % path
		start = time.time()
		store = cPickle.load(open(path))
		stop = time.time()

		try:
			store["name"]
			store["ent1"]
			store["data"]
		except:
			print "ERROR: wrong format, check file content"
			exit(1)
		self.name= store["name"]
		self.ent1 = store["ent1"]
		self.data = store["data"]
		print "Done in %.2fs." % (stop-start)

	def normalize(self):
		l = []
		for i in self.data.values():
			l += list(i[1])
		#print l
		print len(l)
		mu = np.mean(l)
		s = np.std(l)
		print "mu = %f, s= %f" % (mu, s)
		for i in self.data.items():
			self.data[i[0]] = [i[1][0], (i[1][1]-mu)/s]

	def dump(self):
		print "Storing..."
		start = time.time()
		store = {"name":self.name, "ent1":self.ent1, "data":self.data}
		cPickle.dump(store, open("marshalled/"+self.name+".side.nx", "w"))
		stop = time.time()
		print "Done in %.2fs." % (stop-start)

class DataMatrix(object):

	"""
	The input "data" format should be: {(ent1, ent2): value} for all the observed elements in the matrix.

	The format in which the data is stored in the DataMatrix object is the following: 
	featsHT = {domain1Name_numeric : [ numpy16_domain2Names_numeric, numpyX_labels ]}

	"""

	@dispatch(str, Entity, Entity, dict, type)
	def __init__(self, name, ent1, ent2, data, dtype):
		self.name = name
		self.ent1 = ent1
		self.ent2 = ent2
		self.dtype = type
		print "Building features for matrix %s..." % name
		self.data, self.size = NFC.buildPytorchFeatsHT(data, ent1, ent2, dtype)
		print self.data.items()[:3]
		print "Size: ", self.size

	@dispatch(str, Entity, Entity, dict)
	def __init__(self, name, ent1, ent2, data):
		self.name = name
		self.ent1 = ent1
		self.ent2 = ent2
		print "Building features for matrix %s..." % name
		print data.items()[:3]
		self.data = data

	def size(self):
		size = 0
		for i in data.items():
			size += len(i[1][0])
		print "Size: ", size
		return size

	@dispatch( str)
	def __init__(self, path):
		print "Loading %s..." % path
		start = time.time()
		store = cPickle.load(open(path))
		stop = time.time()

		try:
			store["name"]
			store["ent1"]
			store["ent2"]
			store["data"]
		except:
			print "ERROR: wrong format, check file content"
			exit(1)
		self.name= store["name"]
		self.ent1 = store["ent1"]
		self.ent2 = store["ent2"]
		self.data = store["data"]
		print "Done in %.2fs." % (stop-start)

	def normalize(self):
		l = []
		for i in self.data.values():
			l += list(i[1])
		#print l[:100]
		#print len(l)
		mu = np.mean(l)
		s = np.std(l)
		print "mu = %f, s= %f" % (mu, s)
		tmp = []
		for i in self.data.items():
			res = (i[1][1]-mu)/s
			#print res
			#raw_input()
			self.data[i[0]] = [i[1][0], res]
			tmp += res.tolist()
		#print len(tmp)
		#print tmp[:100]
		tmu = np.mean(tmp)
		ts = np.std(tmp)
		#print tmu, ts
		assert abs(tmu) < 0.0001 
		assert abs(ts-1) < 0.0001
		

	def toHashTable(self):
		db = {}
		for p1 in self.data.items():
			#print p1
			i = 0
			assert len(p1[1][0]) == len(p1[1][1])
			while i < len(p1[1][0]):
				db[tuple(sorted([self.ent1[p1[0]], self.ent2[int(p1[1][0][i])]]))] = p1[1][1][i]
				i+=1
		print "Found %d entries" % len(db)
		#print db.items()[:10]
		return db

	def dump(self):
		print "Storing..."
		start = time.time()
		store = {"name":self.name, "ent1":self.ent1, "ent2":self.ent2, "data":self.data}
		cPickle.dump(store, open("marshalled/"+self.name+".nx", "w"))
		stop = time.time()
		print "Done in %.2fs." % (stop-start)

	
