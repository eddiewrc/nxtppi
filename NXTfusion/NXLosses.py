#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  pytorchLossUtils.py
#  
#  Copyright 2018 Daniele Raimondi <daniele.raimondi@vub.be>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import time
import torch as t
from torch.autograd import Variable

class FocalLoss(t.nn.Module):
	
	def __init__(self, alpha=1, gamma=2, logits=True, reduction="sum"):
		super(FocalLoss, self).__init__()
		self.alpha = alpha
		self.gamma = gamma
		self.logits = logits
		self.reduction = reduction

	def forward(self, inputs, targets):
		if self.logits:
			BCE_loss = t.nn.functional.binary_cross_entropy_with_logits(inputs, targets, reduce=False)
		else:
			BCE_loss = t.nn.functional.binary_cross_entropy(inputs, targets, reduce=False)
		pt = t.exp(-BCE_loss)
		F_loss = self.alpha * (1-pt)**self.gamma * BCE_loss

		if self.reduction == "mean":
			return t.mean(F_loss)
		elif self.reduction == "sum":
			return t.sum(F_loss)
		else:
			return F_loss



class LossWrapper():

	"""
	Class that wraps pytorch LogitBCE allowing for ignore index.
	"""

	def __init__(self, loss, type, ignore_index):
		self.loss = loss
		self.ignore_index = ignore_index
		self.type = type
	
	def __call__(self, input, target):
		#t1 = time.time()
		input = input.view(-1)
		target = target.view(-1)
		if self.ignore_index != None:
			mask = target.ne(self.ignore_index)
			input = t.masked_select(input, mask)
			target = t.masked_select(target, mask)
		#t2 = time.time()
		
		r = self.loss(input, target)
		#t3 = time.time()
		#print t2-t1, t3-t2
		#raw_input()
		return r


class CombinedLoss(t.nn.Module):

	def __init__(self, lossList, lossWeights, type):
		self.lossList = lossList
		self.lossWeights = lossWeights
		self.type = type
	
	def __call__(self, input, target):
		tot = 0
		for i, l in enumerate(self.lossList):
			tot += l(input, target)*self.lossWeights[i]
		return tot



class PearsonLoss(t.nn.Module):

	def __init__(self, ignore_index = -1, negative=False):
		self.ignore_index = ignore_index
		self.type = "regression"
		self.negative = negative

	def __call__(self, input, target):
		return self.forward(input, target)

	def forward(self, input, target):
		mask = target.ne(self.ignore_index)
		input = t.masked_select(input, mask.view(-1,1))
		target = t.masked_select(target, mask)

		vx = input - t.mean(input)
		vy = target - t.mean(target)
		cost = t.sum(vx * vy) / (t.sqrt(t.sum(vx ** 2)) * t.sqrt(t.sum(vy ** 2)))
		if self.negative:
			cost = -1 * cost
		return cost
