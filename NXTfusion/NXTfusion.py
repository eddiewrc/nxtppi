#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  pytorchLossUtils.py
#  
#  Copyright 2018 Daniele Raimondi <daniele.raimondi@vub.be>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import numpy as np
import torch as t
from torch.autograd import Variable
from collections import Iterable
from multipledispatch import dispatch

def SafeVariable(v):
	if type(v) == type(None):
		return None
	elif len(v) == 0:
		raise Exception("Very strange to have empty vars here")
	else:
		return v

def f():
	pass

class Entity(object):

	"""
	Class representing the entity concept. It takes a name, the domain (e.g. the proteins UIDs) and automatically it derives the lookup names. Also you can define the numpy type you want to use to save space.
	It is basically a lookup table OR a name list, depending on how you access it.
	"""

	def __init__(self, name, domain, dtype=np.int32):
		assert isinstance(domain, Iterable)
		assert len(domain) > 0
		self.dtype = dtype
		self.name = name
		self.domain = domain
		self.lookup = {}
		i = 0
		while i < len(domain):
			self.lookup[domain[i]] = i
			i+=1
		assert len(self.lookup) == len(self.domain)

	def __next__(self):
		return self.domain.next()

	next = __next__

 	def __iter__(self):
		return iter(self.domain)

	@dispatch(str)
	def __getitem__(self, x):
		return self.lookup[x]
	
	@dispatch(int)
	def __getitem__(self, x):
		return self.domain[x]
	
	def has_key(self, x):
		return self.lookup.has_key(x)

	def __len__(self):
		return len(self.domain)

class Relation(object):
	"""
	Class that represent a relation (matrix in MF terms) with all its parameters and functions.

	
	"""

	def __init__(self, name, type1, domain1, domain2, data, task, loss, relationWeight, pairingFunc=None, side1=None, side2 = None, path=None):
		self.rel = buildRelation(name, type1, domain1, domain2, data, task, loss, relationWeight, pairingFunc=pairingFunc, path=path)

	def __getitem__(self, x):
		return self.rel[x]

	def __delitem__(self, x):
		del self.rel[x]

class RelationPred(Relation):

	"""
	Simplified version of the Relation class, used only for prediction.

	"""

	def __init__(self, domain1, domain2, data):
		assert type(domain1) == Entity
		assert type(domain2) == Entity
		self.rel = {"data":data, "domain1":domain1, "domain2":domain2}

class MetaRelation(object): #it's basically a list with constraints on what you can add

	"""
	This class represent multi-relations between the same entities (tensorial factorization).

	As a convetion, names shoud be in the form ENT1-ENT2, 4 chars for each entity

	The domains must be the same for each relation in it.
	Can allow side info (common to all relations in it).
	"""

	def __init__(self, name,  domain1, domain2, side1=None, side2=None, relations=[], prediction=False):
		self.name = name
		if side2 != None:
			assert len(side2) == len(domain2)
		self.side2 = side2
		if side1 != None:
			assert len(side1) == len(domain1)
		self.side1 = side1
		assert type(domain1) == Entity
		self.domain1 = domain1
		assert type(domain2) == Entity
		self.domain2 = domain2

		self.relationList = []
		for r in relations:
			self.append(r)

	def append(self, r):
		#print type(r)
		assert isinstance(r, (Relation, RelationPred))
		assert self.domain1 == r["domain1"]
		assert self.domain2 == r["domain2"]
		self.relationList.append(r)
	
	def __getitem__(self, x):
		return self.relationList[x]

	def __next__(self):
		return self.relationList.next()

	next = __next__

 	def __iter__(self):
		return iter(self.relationList)

	def pop(self, pos):
		return self.relationList.pop(pos)

	def __len__(self):
		return len(self.relationList)

class ERgraph(list):
	
	def __init__(self, entityList, name = ""):
		self.graph = []
		self.name = name
		self.lookup = {}
		for i, rel in enumerate(entityList):
			assert type(rel) == MetaRelation
			self.graph.append({"name":rel.name, "pos": i, "lenDomain1":len(rel.domain1), "lenDomain2":len(rel.domain2), "arity":len(rel)}) #TODO:add side
			self.lookup[rel.name] = self.graph[i]
		super(ERgraph, self).__init__(entityList)	

	def __getitem__(self, x):
		if type(x) == int:
			return super(ERgraph, self).__getitem__(x)
		elif type(x) == str:
			return self.lookup[x]
		else:
			raise Exception("Unexpected:  ", x)

	def __contains__(self, x):
		if type(x) == int:
			return super(ERgraph, self).__contains__(x)
		elif type(x) == str:
			return x in self.lookup
		else:
			raise Exception("Unexpected: ", x)

	def __str__(self):
		s = "ERgraph:\n-Name:%s\n" % self.name
		for r in self.graph:
			s+= "\tMetaRel:%s, dom1:%d, dom2:%d, arity:%d\n" % (r["name"], r["lenDomain1"], r["lenDomain2"], r["arity"])
		return s

def buildRelation(name, type1, domain1, domain2, data, task, loss, relationWeight, pairingFunc=None, path=None):
	"""
	Function actually transforming the variables into a relation object, checking the consistency of
	their values.


	"""
	assert (pairingFunc == None and path == None) or (pairingFunc != None and path != None)
	d = {}
	assert type(name) == str
	d["name"] = name
	assert type1 == "DS" or type1 == "OD"
	d["type"] = type1
	assert type(domain1) == Entity
	d["domain1"] = domain1
	assert type(domain2) == Entity
	d["domain2"] = domain2
	d["data"] = data
	assert task == "regression" or task == "binary"
	d["task"] = task
	assert relationWeight == "relativeToTarget" or type(relationWeight) == int or type(relationWeight) == float
	d["relationWeight"] = relationWeight
	d["loss"] = loss
	if pairingFunc != None:
		assert type(pairingFunc) == type(f)
		d["pairingFunc"] = pairingFunc
		assert type(path) == str
		d["path"] = path
	return d

def main(args):
	a = ERgraph([1,2,3])
	a.append(4)
	for i in a:
		print i

if __name__ == '__main__':
	import sys
	sys.exit(main(sys.argv))
