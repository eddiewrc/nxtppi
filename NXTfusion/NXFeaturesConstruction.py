#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  PPIpredUtils.py
#  
#  Copyright 2017 Daniele Raimondi <eddiewrc@alice.it>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import random, time, math, torch
import torch as t
import numpy as np


def sortPair(target1, i):
	if target1 < i:
		return target1, i
	else:
		return i, target1

def getOutputsPairs(yp, c):
	return yp[:,c:c+2]

def getOutputs(yp, c):
	return yp[:,c:c+1]

def printableLosses(losses):
	tmp = []
	for i in losses:
		tmp.append(float(i.data))
	assert len(tmp) == len(losses)
	return tmp

def getClassWeights(Ypath, ignore_index=-1):
	labels = [1.0,1.0]
	for i in Ypath:
		if i == ignore_index:
			continue
		labels[i] += 1
	print "Target balancement: (0,1): ", labels
	print "Weights: ", [labels[1]/float(sum(labels)), labels[0]/float(sum(labels))]
	return [labels[1]/float(sum(labels)), labels[0]/float(sum(labels))]

def buildPytorchSide(data, domain, expectedLen = 20, sideDtype=np.float32):
	""" This function builds the data structure containing the side information 
	
		The data structure is a {} indicized with the domain numeric names.

		sideX = {domainName_numeric : numpy32_feats}
	"""
	x = {}
	i = 0
	while i < len(domain):
		x[i] = []
		i+=1

	
	missing = 0
	for i in domain:
		#print i
		try:
			x[domain[i]].append(np.array(data[i], dtype=sideDtype))	
		except:
			x[domain[i]].append(np.zeros(expectedLen, dtype=sideDtype))	
			missing +=1
		#	print i, x[lookup[i]]
		#print len(x[lookup[i]][0]) #== 50
		#raw_input()
	if missing > 0:
		print "WARNING: %d missing proteins in side info." % missing
	return x

def buildPytorchFeatsHT(data, domain1, domain2, relDtype=np.int8):
	""" This function produces the data structure used to pass training data to the
	wrapper.fit function
	
	The data structure is a {(domain1, domain2):VALUE, ... } .
	The format is the following: 
	featsHT = {domain1Name_numeric : [ numpy16_domain2Names_numeric, numpyX_labels ]}

	numpy16 can allocate up to 32k entities instances names
	numpyX labels can have int8 for binary/multiclass (<256 labels)
	
	"""

	size = 0
	x = {}
	i = 0
	while i < len(domain1):
		x[i] = [[],[]]
		i+=1
	if domain1 == domain2:
		print " *** identified as self relation."
		selfRelation = True
	else:
		print " *** identified as asymmetric relation."
		selfRelation = False
	for i in data.items():
		if selfRelation:
			try:
				tmp = tuple(sorted([domain1[i[0][0]], domain2[i[0][1]]])) #probably dangerous here
			except:
				continue
		else:
			tmp = tuple([domain1[i[0][0]], domain2[i[0][1]]])
		x[tmp[0]][0].append(tmp[1])
		x[tmp[0]][1].append(i[1])
	xf = {}

	for i in x.items():
		size += len(i[1][0])
		xf[i[0]] = [np.array(i[1][0], dtype=domain1.dtype), np.array(i[1][1], dtype=relDtype)]
	return xf, size

def buildPytorchFeats(data, domain1, domain2, side1 = None, side2 = None):
	"""
	This function is used for the PREDICTION phase, e.g. it produces
	the x,y, sx1, sx2 data for the wrapper.predict() method
	
	"""
	x = []
	y = []
	sx1 = []
	sx2 = []
	corresp = []
	
	for i in data.items():
		#print i #(('Q96IY1', 'Q9UGM6'), 0)
		#raw_input()
		corresp.append(i[0])
		x.append([domain1[i[0][0]], domain2[i[0][1]]])
		if side1 != None:
			sx1.append(side1[domain1[i[0][0]]])
		if side2 != None:
			sx2.append(side2[domain2[i[0][1]]])
		y.append(i[1])
	assert len(x) == len(y)	
	if side1 == None:
		sx1 = None
	if side2 == None:
		sx2 == None
	if side1 == None and side2 == None:
		return x, y, corresp
	else:
		return x, y, sx1, sx2, corresp



def main():
	
	return 0

if __name__ == '__main__':
	main()

